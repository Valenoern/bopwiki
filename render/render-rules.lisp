;;; render-rules.lisp - useful rules for render formats
(defpackage :bop-render-rules
	(:use :common-lisp
		:bopwiki
			;; join-lines
	)
	(:local-nicknames
		(:linerules :bopwiki-linerules)
	)
	(:export
		#:meta-previewless-p
))
(in-package :bop-render-rules)


(defun meta-previewless-p (meta-lines)
;; this function largely exists to serve as an arbitrary symbol for itself
;; that is, meta lines representing discarding replies are marked with it
	(not-null (linerules:metas-with-rel meta-lines "no-replies"))
	;; LATER: allow configuring exact string by registering "no-replies" to this symbol in line-rules
)
