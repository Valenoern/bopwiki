;;; twinned.rules.lisp - line rules useful for twinned format {{{
(defpackage :bop-render-twinned-rules
	(:use :common-lisp :bopwiki)
	(:local-nicknames
		(:table :bopwiki-core-table)
		(:rules :bopwiki-linerules) (:parse :bopwiki-linerules-parse)
		(:render :bop-render-core)
		(:ttemplate :bop-render-twinned-html)
		
		(:pcre :cl-ppcre)
	)
	(:export
		#:rule-parser-choice
		#:rule-fork
))
(in-package :bop-render-twinned-rules)
;;; }}}


(defun rule-parser-choice (line) ; parser that specially marks choices {{{
	(setf
		;; simply extend the functionality of 'rule-parser-label-comment
		line (parse:rule-parser-label-comment line)
		;; specially mark choices so they can be rendered in a choices section
		(meta-line-rel line) 'ttemplate:choices-block-twinned)
	;; capture raw "random" uri to use it twice
	(let ((raw-value (meta-line-value line)) value)
	
	;; extract information using regular expression
	(pcre:do-register-groups (entry field field-short)
		((format nil
			"^random:~A/~A$"  ; ~A - region of regex
				"([^/]+)" ; $1 - entry
				"((out|back|thread)(links)?)"  ; $2/$3 - field to get links from
			)
			raw-value  nil  :sharedp t)
		
		(unless (null entry)  ; as long as the random URI seems to reference an entry
			(setf  ; make its page list the new value
				value
				(cond
					((equal field-short "out") 'bop-tablerow-outlinks)
					((equal field-short "back") 'bop-tablerow-backlinks)
					((equal field-short "thread") 'bop-tablerow-threadlinks)
					(t nil))
				;; use a meta-line struct to store parsed value,
				;; just because its fields happen to be oddly appropriate
				(meta-line-value line)
					(make-meta-line :value entry :rel value :raw raw-value)
				)
			))
	
	line
)) ; }}}

(defun rule-fork () ; gamebook choice {{{
	(make-line-rule :sphere "meta"  :icon "=>>" :name "fork" :label "choice"
		:displayp t :outlinkp t
		:parser 'rule-parser-choice
)) ; }}}


(rules:register-line-rules (list 'rule-fork))
