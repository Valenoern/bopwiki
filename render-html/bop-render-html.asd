(defsystem "bop-render-html"
	:author "Valenoern"
;	:version "0.0.1"
	:licence "GPL"
	:description "bopwiki html format"
	
	:depends-on (
		"uiop"
		"osicat"  ; for making symlinks
		"html-template"  ; because escaping html entities is ridiculously hard
		"bopwiki" "bop-render"
	)
	:components (
		(:file "stylesheet")  ; :bop-render-html-style
		
		(:file "template")  ; :bop-render-html-template
		(:file "entry-footer")  ; :bop-render-html-footer
		(:file "entry-body")  ; :bop-render-html-copy
		(:file "entry-page"  ; :bop-render-html-page
			:depends-on ("template" "entry-body" "entry-footer"))
		
		(:file "render-html" :depends-on ("stylesheet" "entry-page"))
))
