;;; entry-footer.lisp - page dates etc {{{
(defpackage :bop-render-html-footer
	;; import print l only when debugging so grep shows where debugging is
	;; (:import-from :grevillea #:rintl)
	(:use :common-lisp :bopwiki :bop-render-core)
	(:local-nicknames
		(:table :bopwiki)
		(:render :bop-render-core)
		;; (:uiop) (:html-template)
	)
	(:export
		#:html-page-date
		#:meta-link-html #:attach-link-html #:html-meta-line
		;; meta-comment-html
		#:html-meta-block
))
(in-package :bop-render-html-footer)
;;; }}}


(defun html-page-date (id) "format an html date tag" ; {{{
	;; LATER:
	;; - allow different configurable format strings for date
	;; - simply get a structure containing date format, 'afraid bluebird', etc
	(let ((row (page-row id)) (url (exported-page-url id "htm"))
		placeholder title date date-string
	)
	(setq
		title (bop-tablerow-title row)
		date (bop-tablerow-date row)
	)
	;; fill in title if id is 0
	(unless (> id 0)  (setq id title))
	
	
	;; LATER: print correct date
	(multiple-value-bind (sec minute hour day month year) ; weekday dst-p tz
		(decode-universal-time (unix-to-universal-time date))
		(multiple-value-bind (pron-tts pron-words)
			(render:pronounce-entry-id id :visual nil)
		
		(let (
			(date-title "entry date")  ; LATER: localise output
		)
		(setq
			date-string
			(if (not-null (setq placeholder (render:page-date-placeholder date)))
				(format nil
					"<span title=\"~a\">~a</span>" date-title placeholder)
				(format nil "<time datetime=\"~d-~2,'0d-~2,'0dT~2,'0d:~2,'0d:~2,'0dZ\" title=\"~a\">~2,'0d <b aria-hidden=\"true\">/</b> ~2,'0d <b aria-hidden=\"true\">/</b> ~d</time>"
					year month day hour minute sec  ; datetime: YYYY-MM-DDThh:mm:ssTZD
					date-title
					day month year))
		))
		
		;; output link tag
		(format nil
		"<a href=\"~a\" aria-label=\"dated ~d ~d ~d; entry ~a\">~a &ndash; <span title=\"entry ID - TTS reading: ~a\">~d</span></a>"
			url
			day month year  pron-tts
			date-string
			;; --
			(render:pronounce-entry-id id :visual t :words pron-words)  ; tts reading
			id)  ; id number
	))
)) ; }}}

(defun meta-link-html (icon title text  &key label) ; {{{
	(let (url
		(value text) (visual-label text)
		(numeric-id (bop-tablerow-idno (table:page-row text)))
	)
	
	;; fill in empty labels with integer id, non-zerofilled
	(when (> numeric-id 0)
		(setq
			value numeric-id
			visual-label numeric-id)
	)
	(if (null label)
		(setq label value)
		(setq visual-label label))
	
	(setq url (exported-page-url value "htm"))  ; url of context link
	
	
	(format nil
"		<b title=\"~a\" aria-hidden=\"true\">~a</b> <a href=\"~a\" aria-label=~W>~a</a>"
			title icon  url
			;; escape link labels with quotes to avoid aria-labels getting horribly busted
			(html-template:escape-string
				(format nil "~a: ~a" title label))
			visual-label)
	
	;; note: having the url zerofilled is correct behaviour, at least when entries are currently output to zerofilled filenames
	
	;; BUG: aria labels are busted because of quotes
)) ; }}}

(defun attach-link-html (icon title name) ; {{{
	(join-str
"
		<figure>
		<figcaption><b title=\"" title "\" aria-hidden=\"true\">" icon "</b> <a href=\"#\" aria-label=\"" title ": " name "\">" name "</a></figcaption>
		<img src=\"./media/" name "\" />
		</figure>"
	)
) ; }}}

(defun html-meta-line (line) "render meta line to html" ; {{{
	(let (safe-value value-label icon caption outback
		(kind (meta-line-kind line))
		(meta-link-func (table:get-alternate-function 'meta-link-html))
	)
	
	;; read line information
	(let ((label (meta-line-label line)))
		;; if line includes a visual label use it, otherwise use :value
		(setq
			safe-value
				(format nil "~a" (meta-line-value line))
			value-label
				(if (null label)
					safe-value  label)
			))
	
	;; read rule information
	(let ((rule (funcall (meta-line-kindsym line))))
		(setq
			caption (line-rule-label rule)  ; friendly label, "context" etc
			outback (line-rule-displayp rule)
			;; escape possible "<"/">" in meta line icon
			icon  (html-template:escape-string (line-rule-icon rule))
			))
	
	(cond
		((not-null outback)  ; if this link should be displayed
			(funcall meta-link-func icon caption safe-value :label value-label))
		((equal kind "obj")  ; LATER: test this in a better way
			(attach-link-html icon caption safe-value))
		(t '())
		)
)) ; }}}

(defun meta-comment-html (icon title value) ; currently unused {{{
	(format nil "	<!-- ~a ~a: ~a -->" icon title value)
) ; }}}

(defun html-meta-block (full-body is-reply parent) ; render all meta lines {{{
	(let (obj-lines meta-block
		(excerpt-label "excerpt") (emptying-rel "empty")
		(will-excerpt (will-excerpt-p full-body :is-reply is-reply))
	)
	
	;; do not hide parent link if this is not a reply
	(when (null is-reply)  (setq parent nil))
	
	;; replace meta block with excerpt's meta block if applicable
	(let ((excerpt (bop-page-excerpt full-body)) meta)
		(unless (or
			(null will-excerpt) (null excerpt)  (null (setq meta (bop-page-meta excerpt)))
			)
			(setf (bop-page-meta full-body) meta)
	))
	
	;; for each meta line and what kind it is, collect or discard
	(let ((meta (bop-page-meta full-body)))
	(dolist (line meta) ; {{{
		(let ((kind (meta-line-kind line)) (value (meta-line-value line)) canonical-value str)
		(setq canonical-value (table:page-id value))
		
		;; render meta line unless...
		(unless (or
				;; - it links to immediate parent
				(equal canonical-value parent)
				;; - it's doing something special, like adding an excerpt or clearing the meta section
				(equal (meta-line-rel line) excerpt-label)
				(equal (meta-line-rel line) emptying-rel)
				(equal (meta-line-rel line) "no-replies")
			)
			(setq str (html-meta-line line))
		)
		
		(cond
			;; if it's scrapped or otherwise empty, discard
			((null str) '())
			;; if this is attachment, isolate to end of meta section
			((equal kind "obj")
				(setq obj-lines (cons str obj-lines)))
			;; otherwise add it and continue
			(t (setq meta-block (cons str meta-block))
		))
	))) ; }}}
	
	(unless (null meta-block)  ; clean up meta block {{{
		(setq meta-block (join-str
"	<div>
"
		(join-str-with (nreverse meta-block)
"<br />
"
		)
"	</div>"))
	) ; }}}
	(unless (null obj-lines)  ; if not empty, clean up attachment list {{{
		(setq obj-lines (nreverse obj-lines))
		(setq obj-lines
			(join-str
			(nn) "	<div class=\"attachments\">"
			(join-lines obj-lines)
			"	</div>"))
	) ; }}}
	;; if neither is empty, join all meta & attachments {{{
	(unless (and (null meta-block) (null obj-lines))
	(join-str
"	<div class=\"meta\">
" meta-block obj-lines "
	</div>"
	))
	;; }}}
)) ; }}}
