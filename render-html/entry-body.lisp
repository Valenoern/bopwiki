;;; entry-body.lisp {{{
(defpackage :bop-render-html-copy
	(:use :common-lisp :bopwiki :bop-render-core)
	(:local-nicknames
		(:bop :bopwiki) (:table :bopwiki)
		(:render :bop-render-core)
		(:uri :bop-render-uri)  ; incomplete experiment to move render-AP uris to render
		(:template :bop-render-html-template)
		;; uiop
	)
	(:export
		;; render-line-html
		;; meta-links-plain
		#:format-body-html
))
(in-package :bop-render-html-copy)
;;; }}}


(defun render-line-html (line-core kind  &key newline) ; {{{
	;; LATER: make this less hardcoded to allow for rule customisation
	(let ((nn newline))

	(cond
	((equal kind "h1")  (format nil "<h1>~a</h1>~a" line-core nn))
	((equal kind "h2")  (format nil "<h2>~a</h2>~a" line-core nn))
	((equal kind "hr")  (format nil "<hr />~a" nn))
	
	((equal kind "li-on")
		(format nil "<ul>~a<li>~a</li>~a" nn line-core nn))
	((equal kind "li-off")
		(format nil "<li>~a</li>~a</ul>~a" line-core nn nn))
	((equal kind "li")  (format nil "<li>~a</li>~a" line-core nn))
	((equal kind "li-x")
		(format nil "<li class=\"checked\"><b class=\"skeuo\">🗴</b> ~a</li>~a" line-core nn))
	
	((equal kind "blockquote-on")  (format nil "<blockquote>~a" nn))
	((equal kind "blockquote-off")  (format nil "</blockquote>~a" nn))
	((equal kind "pre-on")  "<pre>")
	((equal kind "pre-off")  (format nil "</pre>~a" nn))
	((equal kind "pre")  (format nil "~a~a" line-core nn))
	;; LATER: somehow remove newline from bottom of pre tag
	
	(t (format nil "~a<br />~a" line-core nn))  ; should include 'escaped-line'
))) ; }}}

(defun meta-links-plain (full-body  &key author) ; meta lines for fediverse {{{
"Return a simple meta block suitable for text-only fediverse platforms like mastodon

FULL-BODY - a BOP-PAGE struct"

	(let (
		(meta (bop-page-meta full-body)) (newline (format nil "<br />")) (result "")
	)
	
	(dolist (meta-line meta)
		(let (web-link ; separator kind value {{{
			(separator
				(if (equal result "")  "" newline))
			(kind (meta-line-kind meta-line)) (value (meta-line-value meta-line))
		) ; }}}
		(when (or
			;; LATER: un-hardcode by getting a 'characteristic' of meta line
			(equal kind "fwd")
			;;(equal kind "full")  ; some of these lead back to the fediverse
			;;(equal kind "cxt")  ; in fediverse threads, parent links are a bit confusing
			)
			
			(setq
				web-link
					(render:entry-web-url value :author author :filetype "htm")
				;; LATER: handle bogus urls
				web-link (uri:render-web-link web-link)
			)
			
			(setq result
				(format nil "~a~a<a href=\"~a\">~a</a>"
					result separator  web-link web-link
			))
			;; it's unknown if links can be given aria-labels on mastodon,
			;; so simply leave off meta line icons and print raw link
			;; LATER: see if links can safely be given friendly display text
	)))
	
	;; return nil for an empty result to make testing for it easier
	(if (equal result "")  nil  result)
)) ; }}}

(defun format-body-html ; output pretty html entry {{{
	(full-body is-reply  &key newline absolute-url use-title meta-links author)
"output pretty entry" 

	(let ((body-lines (bop-page-body full-body)) parsed-lines ; {{{
		;; several metadata items are stored inside full-body
		;(title (bop-page-title full-body)) (id (bop-page-id full-body))
		(source-path (bop-page-path full-body)) url
		(body-rules (bop-tablerow-bodyrules (bop-page-tablerow full-body)))
		;; these cutoff values can be set in init.lisp
		(excerpt-limit bop-render-core:*post-excerpt-limit*)
		(cutoff-limit bop-render-core:*post-cutoff-limit*)
		(excerpt (bop-page-excerpt full-body))
		;; use \n if no custom line separator given
		(line-separator (if (null newline) (nn) newline))
	)
	;; for the purposes of things like atom feeds or activitypub, absolute urls can be passed
	(if (null absolute-url)
		(setq url (exported-page-url nil "htm"  :filename source-path))
		(setq url absolute-url))
	;; }}}
	
	;; excerpt and header - :is-reply, :use-title {{{
	;; if page has a usable prewritten summary, replace body-lines with that summary
	(when (and
		(will-excerpt-p full-body :is-reply is-reply) (not-null excerpt)
		)
		(setq body-lines (bop-page-body excerpt))
	)
	
	;; if this is a full-length entry, or syndicated somewhere, add generated header
	(when (or (null is-reply) (equal use-title t))
		(setq body-lines (bop-page-body (add-header-line full-body)))
	)
	;; }}}
	
	;; parse each line in body {{{
	(let (cutoff (body-len 0) line kind in-block line-core)
	(loop for i  from 0 to (- (length body-lines) 1)  do
		(setq line (nth i body-lines))
		;; parse information out of line
		(let ((parse-data (parse-line-kind line in-block :enabled body-rules))) ; {{{
			(setq kind (formatting-result-kind parse-data))
			;; update what block is active or not
			(setq in-block (formatting-result-indented parse-data))
			;; get text content of line from parse result
			(setq line-core (formatting-result-payload parse-data))
		) ; }}}
		
		;; calculate how long body would be if this line was added
		(setq body-len (+ (length line) body-len))
		;; while under one of the limits, add line to body
		(when (or (null is-reply) (< body-len excerpt-limit) (< body-len cutoff-limit)) ; {{{
			(setq parsed-lines (cons (render-line-html line-core kind :newline line-separator) parsed-lines))
			
			(when (and is-reply (null cutoff) (> body-len cutoff-limit))
				(setq cutoff i)
		)) ; }}}
	)
	;; chop down into automatic excerpt if applicable
	(when (and (null is-reply) (not (null cutoff))) ; {{{
		;; (subseq str n) gives lines after an element
		;; but parsed-lines is backwards, so this needs to count from its end
		(setq parsed-lines (subseq parsed-lines (- (length body-lines) cutoff)))
	)) ; }}}
	;; }}}
	
	;; if lines were chopped off anywhere above, add cut
	(when (< (length parsed-lines) (length (bop-page-body full-body))) ; {{{
		(unless (line-is-empty (car parsed-lines))
			(setq parsed-lines (cons "" parsed-lines))
		)
		(setq parsed-lines (cons (template:cutoff-line-html url) parsed-lines))
	) ; }}}
	
	;; join lines together, with line separator already added during line renders
	(setq parsed-lines (join-str-with (nreverse parsed-lines)  ""))
	
	;; mainly for the fediverse, allow tossing meta lines onto rendered body
	(let ((line-break "<br />") meta-lines) ; {{{
	(unless (or (null meta-links) (null author)) ; :author is required with :meta-links
		(setq meta-lines (meta-links-plain full-body :author author))
		
		(unless (null meta-lines)
			(setq parsed-lines
				(format nil "~a~a~a~a"
					parsed-lines line-break line-break meta-lines)))
	)) ; }}}
	
	parsed-lines
)) ; }}}
