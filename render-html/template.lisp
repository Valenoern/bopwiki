;;; template.lisp - html boilerplates {{{
(defpackage :bop-render-html-template
	(:use :common-lisp
		:bopwiki
			;; join-lines
		:bop-render-core
			;; rendered-entry-...
	)
	(:local-nicknames
		(:table :bopwiki-core-table)
		(:linerules :bopwiki)
		(:render :bop-render-core)
	)
	(:export
		#:cutoff-line-html
		#:nesting-glyph-html #:raw-page-html #:html-meta-list
		#:html-entry-template #:html-replies-section
		
		#:html-title
		#:html-template
		#:html-thread-page
))
(in-package :bop-render-html-template)
;;; }}}


;; functions for within entry box {{{

(defun cutoff-line-html (url) "return a 'cut' at the end of an html excerpt" ; {{{
	(format nil
	"<a class=\"cut\" href=\"~a\"><b>...</b></a>"
	url
)) ; }}}

(defun nesting-glyph-html (nesting) ; nesting indicator {{{
"display nesting indicator for unstyled pages or small screens"
	(let (
		(nesting-level (if (null nesting)  0 nesting))
		(nest-character "▌") (nest-description "reply level ~a")
	)
	
	(cond
		((< nesting-level 1) "")
		(t
		
		(let ((title
			(format nil nest-description nesting-level)))  ; "reply level n"
		(format nil
"<p class=\"nesting\"><span aria-label=\"~a\" title=\"~a\">~a</span></p>
"
			title title
			;; print nesting character NESTING-LEVEL times
			;; LATER: explain how this even works
			(format nil "~v@{~A~:*~}" nesting-level nest-character))
	)))
)) ; }}}

(defun raw-page-html (full-body replyp) ; entry sourcefile container {{{
"container to add entry source to entry webpage"
	(let ((ext
		(bop-page-ext full-body)))
	
	;; as it can be rather long, omit entry source from entries below top one
	(when (null replyp)
		(format nil
"<!-- raw source file~A from bopwiki folder -->
<template>
<textarea>~A</textarea>
</template>
" ; necessary break - see html-entry-template
		
		;; show file extension as long as there is one
		(if (null ext)  ""  (format nil " (.~A)" ext))
		
		(render:reconstruct-raw-page full-body)
	))
	
	;; note: in html5, the only "raw text" element which allows you to include almost all "<"/">" unescaped is textarea.
	;; its one restriction is you can't include a string like "</textarea"
	;; see: https://dev.w3.org/html5/spec-LC/syntax.html#raw-text-elements
)) ; }}}

(defun html-meta-list (meta-lines  &key classname) ; particular subgroup of meta lines {{{
	(if (null meta-lines)
		""
		(format nil
"	<div~A>
~a
	</div>"
			(if (null classname)
				""  (format nil " class=\"~a\"" classname))
			(join-str-with meta-lines
"<br />
"
				))) 
) ; }}}

;; }}}

;; functions for around or outside entry {{{

(defun html-entry-template (entry) ; article tag / entry box {{{
	(let (  ; raw meta-block nesting {{{
		(raw (rendered-entry-raw entry))
		(meta-block (rendered-entry-metablock entry))
		(nesting
			(nesting-glyph-html (rendered-entry-nesting entry)))
	) ; }}}
	
	(format nil
"

~A
<article id=\"page_~a\">

~A<div class=\"copy\">
~A</div>

<footer>
	<p>~A~A</p>
~A
</footer>
</article>"
		;; entry divider or nothing
		;; these are usually hidden but improve readability with styles disabled
		(if (rendered-entry-isreply entry)
			(format nil "<hr />~%")  "")
		
		(rendered-entry-id entry)  ; page_~a
		
		;; raw page or nothing, plus nesting indicator
		(if (null raw)
			nesting
			(format nil "~a~a~a" raw nesting #\newline))
		
		(rendered-entry-body entry)  ; body/copy
		(rendered-entry-date entry)  ; footer date
		
		(if (rendered-entry-summarised entry)
			" <span class=\"entry-info-summarised\">(summary)</span>"
			"")
		
		;; meta block or nothing
		(or meta-block "")
))) ; }}}

(defun html-replies-section (replies) ; all previews connected to entry {{{
	;; flatten replies to string
	(if (null replies)
		""
		(format nil
"
<div class=\"replies\">~a

</div>"
			(join-str replies))
)) ; }}}


(defun html-title (pagename) ; page head title {{{
	(let ((id
		(table:page-id pagename)))
	(format nil "microwiki output ─ page ~a" id)
	;; LATER: 
	;; - allow swapping out this function
	;; - finish 'newbird' and use it for more accessible titles
)) ; }}}

(defun html-template (title body) "boilerplate for rendering into an html page" ; {{{
	(format nil
"<!doctype html>
<html><head><meta charset=\"utf-8\" />
	<meta name=\"viewport\" content=\"initial-scale=1\">
	<title>~a</title>
	<link href=\"./style.css\" rel=\"stylesheet\" />
</head><body>
<main>~a
</main>
</body></html>~%"
	title
	body
)) ; }}}

(defun html-thread-page (pagename body replies) ; top level of entry's webpage/thread {{{
	;; get alternate function if defined
	(let ((html-template-func
		(table:get-alternate-function 'html-template)
		))
	
	;; LATER: I don't quite like this funcall syntax; address that later
	(funcall html-template-func
		(html-title pagename)
		(format nil "~a~a"
			body
			(or replies ""))  ; display replies or nothing
	)
)) ; }}}

;; }}}
