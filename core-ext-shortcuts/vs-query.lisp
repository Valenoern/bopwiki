;; shortcuts mainly for init.lisp query functions
(defpackage :bop-valenoern-shortcuts-query
	(:use :common-lisp
		:bop-render-targets  ; bop-render publish targets
	)
	;;(:local-nicknames )
	(:export
		#:dir #:tag
	)
)
(in-package :bop-valenoern-shortcuts-query)


(defun dir (name) (dir-pages name))
(defun tag (name) (tag-pages name))
