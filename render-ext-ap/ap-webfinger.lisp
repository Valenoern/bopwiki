;;; ap-webfinger.lisp - webfinger file pointing to where activitypub was exported {{{
;; webfinger standards information: https://datatracker.ietf.org/doc/html/rfc7033
(defpackage :bop-activitypub-webfinger
	(:use :common-lisp
		:bopwiki
			;; pathname-inside
		:bop-render-core :bop-render-targets
		:bop-render-activitypub-actor
		;:bop-render-activitypub
	)
	(:local-nicknames
		;; (:uiop :uiop)
		(:render :bop-render-core)
		(:actor :bop-render-activitypub-actor)
	)
	(:export
		#:*publish-webfinger-path* #:publish-webfinger-path
		;;ap-actor-webfinger
		#:publish-bop-webfinger
))
(in-package :bop-activitypub-webfinger)
;;; }}}


(defparameter *publish-webfinger-path* nil) ; {{{

(defun publish-webfinger-path (path)
"Set directory to publish rendered entries to when running 'bop publish', using the lisp pathname PATH.
This function is designed to be used in init.lisp, and to nest any arbitrary function to calculate the pathname, ex. (namestring \"/path/to/output\")"
	(let ((path-object (uiop:ensure-pathname path)))
	(setq *publish-webfinger-path* path-object)
))
;; }}}


(defun ap-actor-webfinger (actor-info output) ; signpost to actor object {{{
	(let (actor webfinger ;path
		;; export to path - /output-path/.well-known/webfinger
		(well-known (pathname-inside output ".well-known"))
		;; author account information
		(username (ap-user-handle actor-info)) (host (ap-user-host actor-info))
		(actor-id (ap-user-id actor-info))
	)
	(setq webfinger (make-pathname :type nil :name
		(format nil "webfinger_resource--acct:~a@~a"
			username
			(puri:render-uri host nil))
	))
	
	(setq actor (format nil
"{ \"subject\": \"acct:~a@~a\",  \"links\": [{ \"rel\": \"self\",  \"type\": \"application/activity+json\", \"href\": \"~a\" }]  }"
		username host actor-id))
	
	(ensure-directories-exist well-known)
	(export-in-dir well-known webfinger actor)
	
	actor  ; return json object
)) ; }}}

(defun publish-bop-webfinger (target cwd) ; publish activitypub info to webfinger directory {{{
;; this can be used in init.lisp, and to publish multiple bop blogs to the same domain
;; LATER: document how complete this is. currently you at least have to manually install the .conf for nginx, possibly other things

	(let (output)
	target cwd  ; unused required arguments, leave alone
	
	;; if no publish path, use the bop-render one
	(when (null *publish-webfinger-path*)
		(publish-webfinger-path render:*default-publish-path*)
		(format t "WARNING: webfinger publish path not set. It should be at the root of your rendered website~%")
	)
	
	;; create directory, & export to user-configured directory
	(setq output (uiop:ensure-pathname *publish-webfinger-path*))
	(ensure-directories-exist output)
	
	(ap-actor-webfinger (ap-actor-info) output)
)) ; }}}
