;;; entry-copy.lisp - entry's main content to be rendered
(defpackage :bop-render-gemini-copy
	(:use :common-lisp :bopwiki
		:bop-render-core :bop-render-targets
	)
	(:local-nicknames
		(:table :bopwiki) (:render :bop-render-core)
	) ; uiop
	(:export
		#:format-body-gemini
))
(in-package :bop-render-gemini-copy)


(defun format-body-gemini (full-body)
	(let (body)
	(setq
		body (bop-page-body full-body)
		;; other necessary things like excerpt length would go here
	)
	
	(join-lines body)
	
	;; LATER: actually process formatting rules - issues.bop/1632868887
))
