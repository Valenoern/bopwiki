(defpackage :bop-render-gemini-filenames
	(:use :common-lisp :bopwiki :bop-render-core)
	(:local-nicknames  (:table :bopwiki) (:render :bop-render-core)
		)
	(:export
		#:page-source-filename
))
(in-package :bop-render-gemini-filenames)


(defun page-source-filename (page-id)
	(let ((row (table:page-row page-id)) title id-string)
	(setq
		title (bop-tablerow-title row)
		id-string
		(if (> page-id 0)
			page-id  title)
		;; LATER: move this function into core as 'idno-or-title
	)
	(make-pathname :name (format nil "~a_source" id-string) :type "gmi")
))
