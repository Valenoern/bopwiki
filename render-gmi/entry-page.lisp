;;; render-gmi.lisp - gemini renderer {{{
(defpackage :bop-render-gemini-page
	(:use :common-lisp :bopwiki :bop-render-core :bop-render-targets
		:bop-render-gemini-copy
		:bop-render-gemini-footer
		:bop-render-gemini-filenames
	)
	(:local-nicknames  (:table :bopwiki) (:render :bop-render-core)
		(:c0py :bop-render-gemini-copy)
			;; the name 'copy' is strictly not allowed for unknown reasons
		(:footer :bop-render-gemini-footer)
	) ; uiop
	(:export
		;; gmi-entry-template
		#:render-entrypage-gemini
		#:raw-page-gemini
))
(in-package :bop-render-gemini-page)
;;; }}}


(defun gmi-entry-template (body page-date meta-block  &key source-link) ; {{{
	(let (
		;; LATER: allow localising label
		(source-label "View entry source (not text-to-speech friendly)")
		;; add padding to meta block only if there is one
		(meta-block-str
			(if (null meta-block)
				""
				(format nil "~a~%~%" meta-block)))
	)
	
	(format nil
"~a


```metadata
____
```

~a~a


=> ~a  ~a"
		body
		meta-block-str page-date
		source-link source-label
	)
)) ; }}}

(defun raw-page-gemini (full-body) ; {{{
	(let (body filename rendered-link
		(source-title "Entry source") (back-label "Back to page")
	)
	(setq
		body (bop-page-body full-body)
		body (render:reconstruct-raw-page full-body)
		filename (table:bop-page-filename full-body)
		
		rendered-link (render:exported-page-url nil "gmi" :filename filename)
	)
	
	body
	(format nil
"# ~a

```
~a
```

=> ~a  ~a
"
		source-title
		body rendered-link
		back-label
	)
)) ; }}}


(defun render-entrypage-gemini (pagename  &key body) ; {{{
	(let ((full-body
			(if (null body)  ; allow passing BODY for testing etc
				(page-body pagename)  body))
		page-id page-date meta-block entry-copy source-link
	)
	
	(unless (null full-body)
		(setq
			page-id (bop-page-id full-body)
			
			;; render some things
			page-date (footer:gmi-page-date full-body)  ; rendered page date
			meta-block (footer:gmi-meta-block full-body)  ; rendered meta block
			entry-copy (c0py:format-body-gemini full-body)  ; rendered entry body
			
			source-link (page-source-filename page-id)
	))
	
	;; plug rendered sections into template
	(gmi-entry-template  entry-copy page-date meta-block :source-link source-link)
)) ; }}}


;; LATER: think about threading entries
