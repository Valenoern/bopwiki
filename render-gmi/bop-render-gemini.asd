(defsystem "bop-render-gemini"
	:author "Valenoern"
;	:version "0.0.1"
	:licence "GPL"
	:description "bopwiki gemini format"
	
	:depends-on ("uiop" "bopwiki" "bop-render")
	:components (
		;; package names: bop-render-gemini-...
		
		(:file "filenames")
		
		(:file "entry-copy")  ; :...-gemini-copy
		(:file "entry-footer")  ; :...-gemini-footer
		(:file "entry-page" :depends-on ("filenames" "entry-copy"))  ; full webpage - :...-gemini-page
		;; in this system 'footer' is handled separately for no particular reason
		
		(:file "render-gemini" :depends-on ("entry-page"))  ; :bop-render-gemini
))
