;;; helper functions for tests {{{
(defpackage :bopwiki-test-helpers
	(:use :common-lisp :bopwiki :fiveam)
	(:local-nicknames
		(:table :bopwiki) (:entryfile :bopwiki-core-entryfile)
		(:rules :bopwiki-linerules)
	)
	(:export
		;; pagetable
		#:empty-pageattr
		#:example-directories #:example-page-file #:read-example-page
		#:find-example-pages
		
		;; test
		#:string-equals-bytes
))
(in-package :bopwiki-test-helpers)
;;; }}}



;;; example page table {{{

(defun empty-pageattr() (make-bop-pageattr :revision nil))

(defun example-directories () ; create example directories {{{
	(find-example-pages)  ; create page tables within bopwiki-core-table
	(setq bopwiki:*top-directory* (parse-namestring "/example/test.bop/")) ; top of fake bop stem
	
	(let ( ; page-1 page-2 page-3 page-4 page-5 {{{
		(page-1 (example-page-file ; /1620977002_page1.txt {{{
			:path (parse-namestring "/example/test.bop/1620977002_page1.txt")
			:id 1620977002 :title "page1"  :tags nil  :bodyrules nil
			:body (list "example page 1")
		)) ; }}}
		(page-2 (example-page-file ; /1620977318_page2.txt {{{
			:path (parse-namestring "/example/test.bop/1620977318_page2.txt")
			:id 1620977318 :title "page2"  :tags nil  :bodyrules nil
			:body (list "example page 2")
		)) ; }}}
		(page-3 (example-page-file ; /dir1/1620977512_page3.txt {{{
			:path (parse-namestring "/example/test.bop/dir1/1620977512_page3.txt")
			:id 1620977512 :title "page3"  :tags nil  :bodyrules nil
			:body (list "example page 3")
		)) ; }}}
		(page-4 (example-page-file ; /dir2/1620977583_page4.txt {{{
			:path (parse-namestring "/example/test.bop/dir2/1620977583_page4.txt")
			:id 1620977583 :title "page4"  :tags nil  :bodyrules nil
			:body (list "example page 4")
		)) ; }}}
		(page-5 (example-page-file ; /dir2/dir3/1620977694_page5.txt {{{
			:path (parse-namestring "/example/test.bop/dir2/dir3/1620977694_page5.txt")
			:id 1620977694 :title "page5"  :tags nil  :bodyrules nil
			:body (list "example page 5")
		)) ; }}}
	) ; }}}
	
	;; put each sample page into table  {{{
	(dolist (page (list page-1 page-2 page-3 page-4 page-5))
		(table:put-page (bop-page-tablerow page))
		(let ((filename (bop-page-path page)) basename)
			(setq basename (pathname-name filename))
			(store-page-directory basename filename)
	))
	;; }}}  dirs: /page1 /page2 /dir1/page3 /dir2/page4 /dir2/dir3/page5
)) ; }}}

(defun example-page-file (&key body id title tags bodyrules path) ; {{{
	(let ((page-body body) (page-ext "txt")
		(page-title title) (page-tags tags) (page-path path) (page-id id) (page-rules bodyrules)
	)
	(cond
		;; if no path, try to construct path as id + title .txt
		((and (null page-path) (not (null id)))
			(setq page-path (name-page-lisp id title page-ext)))
		;; otherwise get extension from path
		((not (null page-path))  (setq page-ext (pathname-type page-path)))
	)
	(make-bop-page
		:path page-path  :ext page-ext
		:id page-id  :title page-title
		:body page-body  :meta nil
		
		:tablerow  (make-bop-tablerow
			:idno page-id  :date page-id  :title page-title
			:filename page-path
			:tags page-tags  :bodyrules page-rules
			:attr (empty-pageattr)
			:linksto nil  :linksfrom nil
)))) ; }}}

(defun read-example-page (pagename  &key id basename) ; {{{
	(let (page-info label)
	(when (and (null id) (null basename))
		(when (null basename)  (setq basename (pathname-name pagename)))
		(setq page-info (split-pagename pagename))
		(setq id (parse-integer (nth 0 page-info)))
		(setq label (nth 1 page-info))
	)
	
	(cond
	;; this repo's initial commit ─ Aug 25 02:35:39 2020 -0800
	((equal id 1598380539)  (example-page-file ; {{{
		:id id  :title label  :bodyrules '("h1")  :body (list
		"commit d31532449696d1232ef1ccb7c0746eaa162a1521"
		"Author: Takumi <takumi@sunrise-peak>"
		"Date:   Tue Aug 25 02:35:39 2020 -0800"
		""
		"    initial commit; new.sh")
	)) ; }}}
	;; a page with 'zero title' ─ no label, date 2010-12-22 15:00:03 Z
	((equal id 1293030003)  (example-page-file
		:id id  :title label  :body (list "- mirror knight -")
	))
	((equal basename "root")  (example-page-file
		:id id  :title label  :body (list "- root node -")
	))
	;; for excerpting / revision tests
	((equal id 1620189405)  (entryfile:parse-page-lines ; {{{
		'("# excerpting"
		""
		"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut dolor eget ipsum sagittis dapibus vel ut odio. Aenean maximus sodales felis non vulputate. Vivamus euismod, lacus ornare auctor fringilla, odio dolor posuere dui, sed lobortis augue lectus vitae leo. Etiam dapibus scelerisque neque, eget fermentum ipsum posuere at. Duis tincidunt convallis urna vitae congue. In malesuada libero quis purus dapibus, et malesuada sem blandit."
		""
		"Ut sit amet lobortis sem. Vestibulum neque ligula, facilisis non tincidunt ut, scelerisque quis tortor. Pellentesque elementum, ipsum et interdum porta, risus ipsum efficitur lorem, ut feugiat nulla lorem eget leo. Quisque in gravida arcu, eget lobortis risus. Cras ac lobortis dui. Aenean bibendum faucibus imperdiet. Vivamus et dolor eget eros porta vestibulum. Nullam velit nibh, tincidunt sed vehicula sit amet, laoreet at nisi. Vivamus faucibus faucibus eros, id imperdiet eros vulputate a. Mauris mollis, metus bibendum iaculis ultrices, justo dolor ornare ipsum, non consequat quam nisl porta felis."
		""
		"@ 4620189405 excerpt")
		:path (make-pathname :name "1620189405_excerpting" :type "txt")
	)) ; }}}
	((equal id 4620189405)  (entryfile:parse-page-lines ; {{{
		'("entry was excerpted")
		:path (make-pathname :name "4620189405-e_excerpting" :type "txt")
	)) ; }}}
	;; for meta line tests
	((equal id 1633848808)  (entryfile:parse-page-lines ; {{{
		(list
			"meta line test" "" ""
		
			"=> 1293030003 zero title"
			"=> 1620189405 excerpting test"
			"<= 1598380539 initial")
		:path (make-pathname :name "1633848808_metas" :type "txt")
	)) ; }}}
))) ; }}}

(defun find-example-pages () ; {{{
	(table:create-tables)  ; create tables normally within :bopwiki-core-table
	(rules:find-meta-rules)
	(let (filenames (page-list nil) (largest-id 0))
	
	;; list of pages to 'read from disk'
	(setq filenames (list
		(make-pathname :name "root" :type "txt")
		(make-pathname :name "1293030003" :type "txt")
		(make-pathname :name "1598380539_initial" :type "txt")
		(make-pathname :name "1633848808_metas" :type "txt")
	))
	
	;; get example pages and put them in table
	(dolist (path filenames)
		(let ((file (read-example-page path)) id)
		
		;; add to table statistics
		(setq page-list (cons (pathname-name path) page-list))
		(setq id (bop-page-id file))
		(when (> id largest-id) (setq largest-id id))
		
		(table:put-page (bop-page-tablerow file))
	))
	;; add table statistics - :bopwiki-core
	(store-table-meta page-list largest-id)
)) ; }}}

;; LATER: most likely if you were making your own bop add-on it would probably have its own example page table functions, given the possibility for the example data in here to update

;;; }}}


(defun string-equals-bytes (a b  &key dump force-fail) ; check fancy strings are equal {{{
"Test whether output string A equals byte vector B,
to allow comparing strings that might contain special characters or terminal escapes.
The exact values being compared will look something like: \"NIL\" -> #(78 73 76); to get this vector from A, use (string-to-octets A).

If FORCE-FAIL is t the test will always fail, allowing a developer to manually mark a test output wrong."
	(let ((bytearray-a (sb-ext:string-to-octets a)) result)
	
	(setq result
	(and  ; if the two byte arrays are equal and not passed :force-fail t, return t
		(equal  (write-to-string bytearray-a)  (write-to-string b))
		(null force-fail)
	))
	
	;; debug reference string when passed :dump t
	(unless (null dump)
		(format t "TEST STRING:~%~a~%" a)
		(when (null result)
			(format t "REFERENCE STRING:~%~a~%TEST STRING'S BYTE ARRAY:~%~a~%" (bytearray-to-string b) bytearray-a))
	)
	
	result
)) ; }}}

;; LATER: see if there's a way to not depend on sb-ext for converting string to octets
