;;; tests for core bop system(s) {{{
(defpackage :bopwiki-test-core
	(:use :common-lisp :bopwiki :fiveam :bopwiki-test-helpers)
	(:local-nicknames (:table :bopwiki))
	(:export
		;;#:list-table-test #:new-page-test #:rename-test
		
		#:run-tests
))
(in-package :bopwiki-test-core)

(def-suite core-tests)
(in-suite core-tests)
;;; }}}


;;; core commands {{{

(defun list-table-test () ; "bop list" {{{
	(find-example-pages)  ; create page tables within bopwiki-core-table
	
	(let ((which-pages (table:non-revision-pages))  (output (open-string))
		)
	
	(with-output-to-string (s output)
	(let ((*standard-output* s))
		(list-pages-table which-pages)
	))
	
	#| reference: (numbers in green)
         0 root
1293030003 
1598380539 initial
1633848808 metas
	|#
	(string-equals-bytes  output  (vector ; {{{
		27 91 51 50 109 32 32 32 32 32 32 32 32 32 48 27 91 48 109 32 114 111 111 116 10 27 91 51 50 109 49 50 57 51 48 51 48 48 48 51 27 91 48 109 32 10 27 91 51 50 109 49 53 57 56 51 56 48 53 51 57 27 91 48 109 32 105 110 105 116 105 97 108 10 27 91 51 50 109 49 54 51 51 56 52 56 56 48 56 27 91 48 109 32 109 101 116 97 115 10
	)) ; }}}
)) ; }}}

(defun new-page-test () ; make new page title as in "bop new" {{{
	(let (
		(new-epoch-short (new-page-title "67"))
		(new-epoch (new-page-title "1608008975"))
		(new-iso (new-page-title "2020-12-15T05:09:35Z"))
		(new-iso-long (new-page-title "2020-12-15T05:09:35.000Z"))
		(new-nickname (new-page-title "test"))
		(unix-clock (get-unix-time))
	)
	
	(and
		(equal new-epoch-short (name-page-lisp "67" nil "txt"))
		(equal new-epoch (name-page-lisp "1608008975" nil "txt"))
		(equal new-iso (name-page-lisp "1608008975" nil "txt"))
		(equal new-iso-long (name-page-lisp "1608008975" nil "txt"))
		(equal new-nickname (name-page-lisp unix-clock "test" "txt"))
	)
)) ; }}}

(defun rename-test () ; rename pages {{{
	(find-example-pages)  ; create page tables within bopwiki-core-table
	
	;; test basically every single way to rename a page
	(let (
        ;; title-by-num = get by number, rename to include title. etc
		(title-by-num (renamed-page-title "1598380539" "first"))
		(nil-by-num (renamed-page-title "1598380539" nil))
		(num-by-num (renamed-page-title "1598380539" "1609076862"))
		(iso-by-num (renamed-page-title "1598380539" "2020-12-27T13:47:42Z"))
		(long-iso-by-num (renamed-page-title "1598380539" "2020-12-27T13:47:42.000Z"))
		
		(title-by-title (renamed-page-title "initial" "first"))
		(nil-by-title (renamed-page-title "initial" nil))
		(num-by-title (renamed-page-title "initial" "1609076862"))
		(iso-by-title (renamed-page-title "initial" "2020-12-27T13:47:42Z"))
	)
	
	(and
		(equal title-by-num (name-page-lisp "1598380539" "first" "txt"))
		(equal nil-by-num (name-page-lisp "1598380539" nil "txt"))
		(equal num-by-num (name-page-lisp "1609076862" "initial" "txt"))
		(equal iso-by-num (name-page-lisp "1609076862" "initial" "txt"))
		(equal long-iso-by-num  iso-by-num)
		
		(equal title-by-title  title-by-num)
		(equal nil-by-title  nil-by-num)
		(equal num-by-title  num-by-num)
		(equal iso-by-num  iso-by-title)
	)
)) ; }}}

;;; }}}


(test basic
	(is (list-table-test) "List pages")
	(is (new-page-test) "Create filenames of new pages correctly")
	(is (rename-test) "Rename page")
)

(defun run-tests ()
	(run! 'core-tests)
)
