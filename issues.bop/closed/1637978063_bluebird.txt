# "afraid bluebird"

[warning: figure descriptions are not implemented yet, and there is a bunch of ASCII mathematics ahead which will read out like gobbledygook.
I wanted to have that fixed before I published anything like this, but it's sadly not, so I have to put up this warning instead.]


while testing bop's html output with screenreaders it's become apparent that unix timestamp numbers are unnecessarily unwieldy in an audio format.

users with good vision can easily just look at the first digit and last three digits to check whether a number is "probably" the same or different, but it would probably be better for everyone to have a pronounceable version of the numbers available, for instance to "bop open" an entry through dictation.

the most obvious way to shorten big numbers is to use a letter-and-number system, like base 36 or 62:

```
1620475314 -> QSSER6  (base 36, 0-9 A-Z)
```
or
```
1620475314 -> 3UCMrZ (base 58, 0-9 a-Z A-Z excluding [0OIl])
0 = (1620475314 - (2* 58^5) - (27 * 58^4) - (11 * 58^3) - (20 * 58^2) - (49 * 58) - 32)
```
or
```
1620475314 -> 1lfLec (base 62, 0-9 a-Z A-Z)
0 = (1620475314 - (1 * 62^5) - (47 * 62^4) - (41 * 62^3) - (21 * 62^2) - (40 * 62) - 38)
```

however, this is limited by how pronounceable the individual digits are.
the "best" letter-and-number base systems include both cases of latin letters, but this is difficult to distinguish over audio. even if screenreaders were really smart, they would have to read a base-62 number like "1 small-L small-F big-L small-E small-C".
and this leaves only the single-case base system, which doesn't necessarily feel optimal.


but, there is an alternate method:
use absolutely gigantic bases and give each digit an english word.

now a big number becomes a format something like:
```
1620475314 -> afraid bluebird rising
```

fortunately the EFF diceware word list already exists.
on this list, words are numbered in something like a base-6 system to suit die rolls:
```
11166 acid
11211 acorn
11212 acquaint
11213 acquire
11214 acre
11215 acrobat
11216 acronym
```

the largest number on the list is line 7776.

using the entire word list as a base, creating a base-SevenGrandEightNinetySevens number, we get:

```
1620475314 -> acclimate spotlight
0 = (1620475314 - (26 * 7776^2) - (6218 * 7776) - 3570)
```


it then may be reasonable for an entry ID to be rendered like:
```
<time datetime="2021-05-08T24:01:54Z"
  aria-label="16-3-14 acclimate spotlight" title="TTS pronunciation: 16...314 acclimate spotlight">
    1620475314 (acclimate spotlight)
</time>
```

this might result in an amusing problem of people optimising entry numbers for better Afraid Bluebird phrases,
but this could be fixed by making sure Afraid Bluebird is never the canonical representation of entry IDs and individual bop folders (stems) can specify their own particular word lists if they feel they can assemble a list that is more likely to create 'fun' phrases.

(personally I'd probably want to try a list that put all the verbs at the top to take advantage of the fact many numbers start with "1" or "2".)


<= 1608963674 closed
@ 4637978063 excerpt
