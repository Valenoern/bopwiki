;;;; rudimentary bopwiki mode

;;; variables
;(defvar foo 0 "documentation")


(defun bop-new ()
  (interactive)
  ;;(message "default dir: %s" default-directory)
  (shell-command "bop new")
  )


(define-minor-mode bopwiki
  "Rudimentary bopwiki mode"
  ;; displayed in modeline when mode is active
  :lighter " bop"		     
  :keymap
  (let
      ((map (make-sparse-keymap)))
    ;; assign commands with (interactive) to keystrokes
    (define-key map (kbd "C-c n") 'bop-new)
    map)
	
  ;; set local player state variables for each m3u buffer
  ;;(set (make-local-variable 'ls-pos) 0)
	
  )

(add-hook 'dired-load-hook
  (lambda ()
    ;;(when (string= (file-name-extension buffer-file-name) "m3u")  )
    (bopwiki +1))
)


;;; to install this plugin copy it into a convenient place such as ~/.emacs.d
;;; & add these to your init file (likely ~/.emacs.d/init.el):
;;; (load "/path/to/bopwiki.el") (require 'bopwiki)
(provide 'bopwiki)
