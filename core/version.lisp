;;; bop version information
(defpackage :bopwiki-version
	(:use :common-lisp :bopwiki-core)
	(:local-nicknames (:core :bopwiki-core))
	(:export
		#:bop-url #:bop-version
		;; version uses core purely for access to bop-dir ?
		;; BUG: not working
))
(in-package :bopwiki-version)


;(defvar *bop-dir*)

(defun bop-url () ; bopwiki repository url {{{
"Returns the url bopwiki source code can be found at, for use in atom feeds, etc.

If you fork or heavily customise bop, it's recommended you update this function.
Notably, if you make some kind of custom module that alters generation of atom feeds, etc., that call this function, you can substitute your new module's url provided its repository links to the version of the core bopwiki program you're using."

	"https://codeberg.org/Valenoern/bopwiki"
) ; }}}

(defun bop-version () ; automatic version number {{{
"Construct an automatic version number of the form \"YYYYMMDD+commit\".
If git commit cannot be found, fall back to a hardcoded version representing a recent 'stable' release."
;; inspired by PKGBUILD files.
	(let ((bop-dir core:*bop-dir*) (year 2020) (month 2) (day 22) (rev "unknown") head commit)
	
	;; attempt to locate the last git commit
	;; LATER: add to paths correctly
	(uiop:chdir bop-dir)
	
	;; cat .git/HEAD
	(setq head  (uiop:frob-substrings
		(uiop:read-file-line ".git/HEAD")  (list "ref: ")
	))
	;; cat .git/refs/heads/unstable (etc), found in .git/HEAD
	(setq commit
		(uiop:read-file-line (format nil ".git/~a" head))
	)
	;; shorten commit hash to 7 characters, as git usually does
	(setq rev (subseq commit 0 7))
	
	;; LATER: actually get date from commit info
	;; BUG: store a version file in program directory when installed as package
	
	(format nil "~a~2,'0d~2,'0d+~a" year month day rev)
)) ; }}}
