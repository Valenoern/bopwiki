;;; core-table - for reading the hash table that holds entry metadata  {{{
(defpackage :bopwiki-core-table
	(:use :common-lisp
		:bopwiki-core-structs
			;; #:bop-page #:make-bop-page  #:bop-page-path #:bop-page-id #:bop-page-title #:bop-page-ext #:bop-page-body #:bop-page-meta #:bop-page-excerpt #:bop-page-tablerow
		:bopwiki-util :bopwiki-uri
		;; bopwiki-core-entryfile, bopwiki-core depend on this package
	)
	(:export
		;; tables
		#:*page-table* #:*page-body-table* #:*alias-table* #:*directory-table* #:*custom-function-table*
		#:create-tables
		
		;; bop-tablerow
		#:bop-tablerow #:bop-tablerow-idno #:bop-tablerow-date #:bop-tablerow-title #:bop-tablerow-filename #:bop-tablerow-tags #:bop-tablerow-bodyrules #:bop-tablerow-attr #:bop-tablerow-linksto #:bop-tablerow-linksfrom #:bop-tablerow-media #:make-bop-tablerow
		#:bop-tablerow-outlinks #:bop-tablerow-backlinks #:bop-tablerow-threadlinks #:bop-tablerow-misclinks
		;; bop-tablerow extras
		#:duplicate-bop-tablerow
		#:bop-tablerow-basename
		
		;; bop-page extras
		#:bop-page-filename #:bop-page-basename
		#:bop-page-outlinks #:bop-page-backlinks #:bop-page-threadlinks
		
		;; reading pagetable
		#:empty-row #:page-row #:page-exists-p
		#:page-id #:page-date #:page-label #:page-filename #:page-basename
		#:get-page-body
		#:all-pages #:greatest-id  #:table-statistic
		#:get-alternate-function
		
		#:non-revision-pages #:pages-from-directory #:pages-with-tag
		#:sort-pages-ascending
		
		#:justify-idno
		
		;; writing to pagetable
		#:put-page-directory #:put-page-body #:put-page-alias #:put-page
		#:subset-tables
		#:put-alternate-function
))
(in-package :bopwiki-core-table)

;; "page tables" that organise entries during each bop command
(defparameter *page-table* nil)  ; holds metadata of each entry
(defparameter *page-body-table* nil) ; caches of page bodies
(defparameter *alias-table* nil)  ; holds possible alternate names for entries
(defparameter *directory-table* nil)  ; holds index of page basenames by filesystem directory

;; experimental
(defparameter *custom-function-table* nil)

(defstruct bop-tablerow  ; entry metadata row for page table {{{
	idno date title filename  tags bodyrules attr
	;; linksto: "<=" linking to here
	;; linksfrom: "<=" linking elsewhere
	linksto linksfrom
	media  ; "attached" links, used by media files, excerpts, etc.
	
	;; TRANSITIONAL - will replace linksto/linksfrom when fully implemented
	outlinks    ; "see also" style link lines
	backlinks   ; "context" style link lines
	threadlinks ; "context" style link lines which were threaded here
	misclinks   ; meta lines that didn't fit in other fields
	
	;; bop-tablerow-p is not used outside this package as it seemed most useful for abstractions like allowing pagenames or rows in page-row
) ; }}}

;; }}}


;;; reading pagetable {{{

;; extensions to bop-tablerow {{{

(defun duplicate-bop-tablerow (row  &key ; outlinks backlinks threadlinks) ; {{{
		(outlinks nil out-supplied-p) (backlinks nil back-supplied-p)
		(threadlinks nil thread-supplied-p))
	
	(let (
		(out    (if (null out-supplied-p)    (bop-tablerow-outlinks row)  outlinks))
		(back   (if (null back-supplied-p)   (bop-tablerow-backlinks row) backlinks))
		(thread (if (null thread-supplied-p) (bop-tablerow-threadlinks row) threadlinks))
	)
	
	(make-bop-tablerow
		:idno        (bop-tablerow-idno row)
		:date        (bop-tablerow-date row)
		:title       (bop-tablerow-title row)
		:filename    (bop-tablerow-filename row)
		:tags        (bop-tablerow-tags row)
		:bodyrules   (bop-tablerow-bodyrules row)
		:attr        (bop-tablerow-attr row)
		:linksto     (bop-tablerow-linksto row)
		:linksfrom   (bop-tablerow-linksfrom row)
		:media       (bop-tablerow-media row)
		;; see "let"
		:outlinks out  :backlinks back  :threadlinks thread
		)
)) ; }}}

(defun bop-tablerow-basename (row)
	(pathname-name (bop-tablerow-filename row)))
;

;; }}}

;; getting row from page table - empty-row , page-row {{{

(defun empty-row () "return a dummy row for a nil page" ; {{{
	(make-bop-tablerow  :idno 0 :title "empty" :filename nil
		:attr (make-bop-pageattr :revision nil)
)) ; }}}

(defun page-row (arg) "try to get page information given ID or name" ; {{{
	(let (safe-arg basename real-row)
	
	;; if an actual tablerow was passed, use it;
	;; otherwise try to look up page's canonical name and get row
	(cond
		((bop-tablerow-p arg)  (setq real-row arg))
		(t
			(setq
				safe-arg (format nil "~a" arg)
				basename (gethash safe-arg *alias-table*)
				real-row
					(gethash (join-str "PAGE:" basename) *page-table*)
				)
	))
	
	(cond
		;; if row not found, return dummy row
		((null real-row)  (empty-row))
		(t  real-row))  ; otherwise return row
)) ; }}}

(defun page-exists-p (arg  &key row) ; if page has filename {{{
"Return whether page has a known filename, as a proxy for whether it 'probably' has a file on disk"
	(let ((page-info
		(if (null row)
			(page-row arg)
			row)
	))
	(not-null
		(bop-tablerow-filename page-info))
)) ; }}}

;; }}}

;; short row accessors: page-id, page-date, page-label, page-filename, page-basename {{{

(defun page-id (arg) "get id number given page name"
	(bop-tablerow-idno (page-row arg)))

(defun page-date (arg) "get display date given page name"
	(bop-tablerow-date (page-row arg)))

(defun page-label (arg) "get nickname given page name"
	(bop-tablerow-title (page-row arg)))

(defun page-filename (arg) "get filename given page name"
	(bop-tablerow-filename (page-row arg)))
	
(defun page-basename (arg) "get basename of filename given page name" ; {{{
	(let ((filename (page-filename arg)))
	(unless (null filename) (pathname-name filename))
)) ; }}}

;; page-body is in core-entryfile.
;; it can't be in this file because it requires code to open page files

;; }}}

(defun get-page-body (pagename) ; get cached body that was put-page-body'd {{{
	(let ((body-table *page-body-table*) row basename)
	(setq
		row (page-row pagename)  ; resolve rows passed in as rows
	)
	
	(when (page-exists-p nil :row row)
		(setq basename
			(bop-tablerow-basename row))
	)
	
	(gethash basename body-table)
)) ; }}}

;; extensions to bop-page {{{

(defun bop-page-filename (full-body)
	(bop-tablerow-filename (bop-page-tablerow full-body)))

(defun bop-page-basename (full-body)
	(bop-tablerow-basename (bop-page-tablerow full-body)))

(defun bop-page-outlinks (full-body)
	(bop-tablerow-outlinks (bop-page-tablerow full-body)))

(defun bop-page-backlinks (full-body)
	(bop-tablerow-backlinks (bop-page-tablerow full-body)))

(defun bop-page-threadlinks (full-body)
	(bop-tablerow-threadlinks (bop-page-tablerow full-body)))
;

;; }}}

;; statistics about page tables {{{

(defun table-statistic (pagetable  &key hash-key set-value) ; get or set a table statistic
"Manipulate a statistic of table PAGETABLE stored under name HASH-KEY. HASH-KEY should be a string that usually starts with 'BOP:'.
If passed SET-VALUE, set it, otherwise get it."
	(if (null set-value)
		(gethash hash-key pagetable)  (puthash hash-key set-value pagetable))
;; LATER: maybe there's reason to export this for extensions?? will have to think about it
)

(defun all-pages (&key set-value) ; get or set all pages list
"Manipulate 'all pages' list of page tables. If present, set it to SET-VALUE, otherwise get it."
	(table-statistic *page-table* :hash-key "BOP:ALL-PAGES" :set-value set-value))

(defun greatest-id (&key set-value) ; get or set widest id
"Manipulate largest id number statistic of page tables. If present, set it to SET-VALUE, otherwise get it."
	(table-statistic *page-table* :hash-key "BOP:MAX-ID" :set-value set-value))
;;
;; }}}

;; querying subsets of pages {{{

(defun non-revision-pages (&key subset) "list all current-version pages" ; {{{
	(let (result
		;; unless page-list is provided, get all entries from page table
		(all-entries (if (null subset)  (all-pages) subset))
	)
	
	(dolist (pagename all-entries)
		(let ((row (page-row pagename)) rev)
		(setq rev (bop-pageattr-revision (bop-tablerow-attr row)))
		
		;; exclude pages which are revisions of others
		(when (null rev)
			(setq result (cons pagename result))
	)))
	
	(nreverse result)
	
	;; LATER: when page table is being cached,
	;;        simply store a list of non-revision pages
)) ; }}}

(defun pages-with-tag (tag) ; {{{
	;; LATER: at some point perhaps move this over to wherever core tags code lives
	(let ((all-entries (all-pages)) result)
	
	(dolist (pagename all-entries)
		(let ((row (page-row pagename)) tags has-tags basename)
			(setq tags (bop-tablerow-tags row))
			(setq has-tags (string-member-p tag tags))
			(unless (null has-tags)
				(setq basename (page-basename pagename))
				(setq result (cons basename result))
			)
	))
	
	result  ; return list of tagged pages as basenames
)) ; }}}

(defun pages-from-directory (dir-path) ; fetch pages inside a directory {{{
	(let (dir-key page-list)
	
	(setq dir-key (format nil "DIR:~a" (namestring dir-path)))
	(setq page-list (gethash dir-key *directory-table*))
	
	page-list
)) ; }}}

(defun sort-pages-ascending (to-sort) ; sort list of page names ascending {{{
"sort list of page names ascending using table of page info"
;; this method *uses* the pagetable but isn't quite a part of it
	(let ((page-list (copy-list to-sort)))
	(stable-sort page-list
		#'(lambda (x y)
		;; compare page ID numbers to sort pages
		;; "root" is always loaded in as number 0, putting it at beginning
		(let ((nx (page-id x)) (ny (page-id y)))
			(when (null nx) (setq nx 0))
			(when (null ny) (setq ny 0))
			(< nx ny)
	)))
)) ; }}}

;; }}}

(defun get-alternate-function (func) ; experimental {{{
	(let (fetched
		(func-table *custom-function-table*))
	
	(setq fetched
		(or 
			(gethash (list 'put-alternate-function func) func-table)
			func))
	
	fetched
)) ; }}}

;;; }}}


(defun justify-idno (id  &key padding) ; justify id according to maximum id in table {{{
"Pad out entry number \"ID\" to the width of the largest known entry number using character PADDING, returning a string. This can be used for right-aligned or zerofilled strings"
	(let ((greatest (greatest-id)) max-length)
	(when (null padding)
		(setq padding #\ ))
	(setq max-length (length (format nil "~a" greatest)))
	
	(format nil "~v,,,v@A"  max-length padding id)
	; ~mincol,colinc,minpad,padchar[@left]A
)) ; }}}


;;; adding data to page table {{{

(defun create-tables () ; {{{
	(setq
		*page-table* (make-hash-table :test 'equal)
		*page-body-table* (make-hash-table :test 'equal)
		*alias-table* (make-hash-table :test 'equal)
		*directory-table* (make-hash-table :test 'equal)
		*custom-function-table* (make-hash-table :test 'equal))
) ; }}}

(defun put-alternate-function (func alternate) ; {{{
	(let ((func-table *custom-function-table*))
	
	;; LATER:
	;; I want to do an experiment where small hash tables can be combined together,
	;; and these "namespace" keys will make it seamless
	(puthash
		(list 'put-alternate-function func) alternate func-table)
)) ; }}}

(defun put-page-directory (basename dir-key) ; {{{
"Put standard entry name BASENAME of entry inside hash table key DIR-KEY representing one of the entry's containing directories"
	(let (in-dir)
	;; add page basename into directory list
	(setq in-dir (gethash dir-key *directory-table*))
	(unless (string-member-p basename in-dir)
		(setf (gethash dir-key *directory-table*) (cons basename in-dir))
	)
)) ; }}}

(defun put-page-body (page) ; cache content of page {{{
	(let ((filename (bop-page-filename page)) basename
		(body-table *page-body-table*)
	)
	(setq basename (pathname-name filename))
	
	(puthash basename page body-table)
	page  ; return full page for the benefit of nesting function
)) ; }}}

(defun put-page-alias (page-key basename  &key alias-table) ; {{{
	(let ((alias-table
		(if (null alias-table) *alias-table* alias-table)))
	
	;; add alias if not empty (this usually happens with nickname/title)
	(unless (null page-key)
		(puthash
			(format nil "~a" page-key)
			basename
			alias-table))
)) ; }}}

(defun put-page (row  &key page-table alias-table) ; add page row to page table {{{
;; used in creating page table and testing dummy page tables
	(let ((id (bop-tablerow-idno row)) safe-id
		(title (bop-tablerow-title row)) (filename (bop-tablerow-filename row))
		basename aliases
		(page-table (if (null page-table) *page-table* page-table))
		(alias-table (if (null alias-table) *alias-table* alias-table))
	)
	(setq safe-id (format nil "~a" id))
	
	;; put plain row into table
	;; this will be filed like "PAGE:1604888065_test" -> S#(row)
	(setq basename (pathname-name filename))
	(puthash (format nil "PAGE:~a" basename) row page-table)
	
	;; alias id, nickname, and filename to page record {{{
	;; ex. 1604888065 -> 1604888065_test, test -> 1604888065_test
	(setq aliases (list
		safe-id  title  basename  (namestring filename)
		(namestring (make-pathname
			:name basename :type (pathname-type filename)
		))
	))
	;; }}}
	
	;; if page has a low number with multiple representations, put in additional aliases
	;; BUG: actually get max ID somehow
	(let ((maximum 2000000000) justified)  ;(greatest-id) {{{
		;; LATER: aliases really needs to be moved to after creating table as max isn't defined >:/
		(greatest-id :set-value maximum)
		(setq justified (justify-idno id :padding #\0))
		(unless (equal justified safe-id)
			(setq aliases (append (list
				justified  (name-page-str safe-id title nil)
			) aliases))
	)) ; }}}
	
	(dolist (page-key aliases)
		;; add alias if not empty (this usually happens with nickname/title)
        (unless (null page-key)
            (puthash page-key basename alias-table)
	))
	
	t  ; not really sure what to return
)) ; }}}

(defun subset-tables (subset) ; {{{
	(let (page-added
		(pages (make-hash-table :test 'equal))
			;; directory table, ALL-PAGES, and aliases are not remade
		(result-subset subset)
	)
	
	(dolist (pagename subset)
		(let ((row (page-row pagename)) medias outlinks)
		
		;; make sure page exists, then add it
		(when (page-exists-p nil :row row)
			(setq
				page-added t
				medias
					(bop-tablerow-outlinks row)
				)
			(put-page row :page-table pages)
			(pushnew pagename result-subset)
			
			;; if this row has an excerpt or other attachments, put them in table
			;; this assumes every attachment will be a 'meta-line.
			(dolist (media medias)
				(let (media-row
					(medianame (meta-line-value media))
				)
				(setq media-row (page-row medianame))
				;; as long as row exists in old table, put it in new tables
				(when (page-exists-p nil :row media-row)
					(put-page media-row :page-table pages)
					;; try not to add excerpts to subset
					;; LATER: allow configuring string for :rel excerpt
					(unless (equal (meta-line-rel media) "excerpt")
						(pushnew (bop-tablerow-basename media-row) result-subset))
				)
			))
	)))
	
	(when (equal page-added t)  ; replace table with subsetted table
		(setq *page-table* pages))
	
	result-subset  ; return subset, in case it was added to
)) ; }}}

;;; }}}
