;;; core.lisp - under reconstruction {{{
(defpackage :bopwiki-core  ; {{
	;; LATER: "deprecated", will be moved to local-nicknames
	(:use :common-lisp
		:bopwiki-core-structs :bopwiki-util :bopwiki-uri
		:bopwiki-core-table
		:bopwiki-filesystem
		:bopwiki-filters
		:bopwiki-oldrules-formatting
	)
	;; as of 2021/09/03, 'official' bop systems are moving toward :local-nicknames instead of :use
	;; this should make it easier to follow what file each function is in.
	(:local-nicknames
		(:bop-fs :bopwiki-filesystem)
		(:table :bopwiki-core-table) (:rules :bopwiki-linerules)
		(:entryfile :bopwiki-core-entryfile)
		(:scanner :bopwiki-core-scanner)
	)
	(:export
		#:load-extensions
		
		#:*bop-dir* #:*top-directory*
		#:*row-filters*
		
		#:parse-month-day #:generated-title
		
		#:*row-filters*
		;; default filters , run default filters , new-row-filters
		
		#:store-page-directory
		#:store-table-meta
		;; store-entry-tablerow , store-table-meta , store-context-links
		#:find-all-pages  ;#::find-all-pages-inner
		
		;; wastebin category
		#:top-line-title
		#:meta-rule #:meta-rule-kind
		#:basic-meta-rules
		
		#:init
)) ; }}
(in-package :bopwiki-core)

;; for storing the directory bop source code is in
(defvar *bop-dir*)
;; for storing the top of current bop stem; mainly for init.lisp
(defparameter *top-directory* nil)

;; }}}


(defun load-extensions (&rest system-list) ; load optional systems if they exist {{{
"Load one or more asdf systems named in SYSTEM-LIST for use as bop extensions, skipping any that were not found. System names should be in the same format as for asdf:load-system.

Example: (load-extensions \"bop-render-publishgroup\")
manual page: issues.bop/1628912743"

	;; for each system in list that is not found, catch error and print warning
	(dolist (sys-name system-list)
		(handler-case (asdf:load-system sys-name)
			(error (c)
				c  ; unused required argument, leave alone
				(format t "Extension was not loaded: ~a~%" sys-name)
		))
	;; you can get information from an error with expressions like  (values 0 c)
	;; but that's not necessary here
)) ; }}}


;;; page header dates - parse-month-day, header-date, generated-title {{{

(defun parse-month-day (idno) "short date displayed in page title" ; {{{
	(let ( ; visual iso universal
		;; if id is a string parse it, otherwise assume it's an integer
		(id (if (stringp idno) (parse-integer idno) idno))
	)
	
	;; decode-universal-time is a little weird and doesn't seem to just output a list
	;; it seems you have to use this macro
	(multiple-value-bind
		(sec minute hour day month)
		(decode-universal-time (unix-to-universal-time id))
		sec minute hour ; year weekday dst-p tz ; leave dummy alone
	
		(list month day)
	)
)) ; }}}

(defun header-date (idno) ; short date in page title {{{
	(let (date month day (separator "."))
	(setq date (parse-month-day idno))
	(setq month (nth 0 date))
	(setq day (nth 1 date))
	
	(format nil "~d~a~2,'0d" month separator day)
)) ; }}}

(defun generated-title(name id) "nickname + date title" ; {{{
	(let (short-date (safe-id (format nil "~a" id))) ; row title
	
	;; create a generated title only if page has a nickname
	(setq short-date (if (or (null name) (equal name "") (equal safe-id "0"))
		nil
		(header-date id)
	))
	
	(unless (null short-date)
		(join-str name " " short-date)
	)
)) ; }}}

;; LATER: this is not really where any of these belong. move them at some point

;;; }}}


;;; helpers to create page table {{{

(defun store-entry-tablerow (filename) ; file entry metadata in page table {{{
	(let (tablerow)
	
	;; get full page object temporarily
	(let (full-body)
		(setq full-body (entryfile:read-page-file filename))
		
		;; run filters added with 'register-table-filter
		(setq
			full-body (rules:run-table-filters full-body)
			tablerow (bop-page-tablerow full-body))
	)
	;; put page in table
	(table:put-page tablerow)
	
	tablerow
)) ; }}}

(defun store-page-directory (basename filename) ; file entries by parent directories {{{
;; collect list of pages within each directory, for use in "bop publish" etc
	(let ((top (find-bop-dir filename)) (dir-list (pathname-directory filename))
		min-len max-len
	)
	;; if no clear top dir can be found,
	;; the loop will run just once on the lowest directory
	(setq max-len (length dir-list))
	(setq min-len max-len)
	(unless (null top)
		(setq min-len (length (pathname-directory top)))
	)
	
	;; for each directory in path up to the top one
	(loop for i  from max-len  downto min-len  do
		(let (dir-key
			(dir (make-pathname :directory (subseq dir-list 0 i)))
		)
		
		;; flatten directory to string to create key
		(setq dir-key  (format nil "DIR:~a"
			(namestring dir)
		))
		
		;; add page basename into containing directory list
		(table:put-page-directory basename dir-key)
	))
	;; LATER: maybe add store-page-directory into put-page
)) ; }}}

(defun store-table-meta (page-list largest-id) ; file statistics in page table {{{
	;; sort all pages list by id number
	(setq page-list (sort-pages-ascending page-list))
	
	(table:all-pages  :set-value page-list)  ; store list of all pages
	(table:greatest-id  :set-value largest-id)  ; store widest id number
) ; }}}

(defun store-context-links (page-list) ; make sure linked pages are connected {{{
	(dolist (page page-list)
		;; get the real row data in case this name is a row alias
		(let ((canonical (table:page-row page)) id linksfrom)
		(setq id (bop-tablerow-idno canonical))
		;; get <= links in this page's meta section
		(setq linksfrom (bop-tablerow-linksfrom canonical))
		
		;; go through each linked page name
		(dolist (link linksfrom)
			(let ((linkrow (table:page-row link)) linksto)
			;; for every <= linked page in list, connect it to the page linking it
			(setq linksto (cons id (bop-tablerow-linksto linkrow)))
			(setf (bop-tablerow-linksto linkrow) linksto)
		))
	))
) ; }}}

;;; }}}

;;; creating page table {{{

(defun find-all-pages-inner (pwd) ; create page table from stem directory {{{
	;; create hash tables to store basic page information
	;; these live in package variables of :bopwiki-core-table
	(table:create-tables)
	(rules:find-meta-rules)
	
	;; get all filenames inside directory - see ./core-scanner.lisp
	(let ((largest-id 0) page-list ; page-names {{{
			(page-names (nreverse
				(scanner:all-page-files (uiop:parse-unix-namestring pwd))
				))
		) ; }}}
	
	;; for each page filename found in wiki dir, create and process
	(dolist (page page-names) ; {{
		(let ((filename (uiop:ensure-pathname page)) tablerow id basename)
		
		;; BUG: store-entry-tablerow is running multiple times?
		(unless (table:page-exists-p nil :row (page-row filename))
			(setq
				;; read tablerow data from entry file and put in table
				tablerow (store-entry-tablerow filename)
				
				;; get info from filename
				id (bop-tablerow-idno tablerow)
				basename (pathname-name filename))
			
			;; store greatest id number
			(when (> id largest-id) (setq largest-id id))
			;; collect page basenames for all-pages
			(setq page-list (cons basename page-list))
			
			;; collect list of pages within each filesystem directory in page table
			(store-page-directory basename filename)
			)
	)) ; }}
	
	;; put metadata about table in table - see above in core.lisp
	(store-table-meta page-list largest-id)
	;; connect context links to and from pages
	(store-context-links page-list)
	
	
	;; NEW RULES: run filters added with 'register-tablepost-filter
	;; for now, this is mostly for updating and replacing 'store-context-links
	;; LATER: convert 'store-context-links and maybe 'store-page-directory into filters
	(dolist (pagename page-list)
		(let ((row (page-row pagename)))
		;; as long as the row was found, run each tablepost-filter on it
		(when (table:page-exists-p nil :row row)
			(dolist (filter rules:*tablepost-filter-list*)
				(put-page (funcall filter row)))
		;; note that it is okay for a tablepost-filter to freely modify the page table, as they are mainly for editing rows /to correspond to other rows/.
	)))
	
	;; debugging aliases
	;;(debug-hash table:*alias-table*)
	
	;; BUG: don't allow empty keys
	
	t
)) ; }}}

(defun find-all-pages (pwd) ; populate page table if not done already {{{
"Put all the pages in a stem directory into page table; if tables already exist, do nothing"
	(let ((have-tables table:*page-table*))
	(when (null have-tables)
		(find-all-pages-inner pwd))
)) ; }}}

;;; }}}


;;; startup for terminal / REPL {{{
;; LATER: REPL use is still largely untested, but appears to be working?

(defun init () ; initialise bop for do-command etc {{{
	(let (
		(stem-dir (uiop:getcwd))
	)
	;; store directory of current bop stem
	(setq *top-directory* (bop-fs:surer-pathname stem-dir))
	
	;; attempt to load this particular folder's init.lisp
	(bop-fs:load-init-file stem-dir)
	
	;; to run command:
	;; (bop:do-command cmd  [:pwd directory]  :wants wants  :action action)
)) ; }}}

;;; }}}
