;;; utility functions {{{
;; elementary stuff that might or might not be replaced with external libraries later
(defpackage :bopwiki-util
	(:import-from :grevillea #:printl)
	(:use :common-lisp)
	(:export
		#:not-null #:printl
		#:nn #:open-string #:list-to-string #:bytearray-to-string #:color-text
		#:join-str-with #:join-str #:join-lines
		#:string-member-p
		
		#:universal-to-unix-time unix-to-universal-time #:get-unix-time
		
		#:puthash #:debug-hash
))
(in-package :bopwiki-util)
;;; }}}


(defun not-null (value) ; {{{
	(not (null value))
) ; }}}


;;; string related - nn, open-string, list-to-string, bytearray-to-string, color-text {{{

(defun nn () "return a newline" ; {{{
"
"
) ; }}}

(defun open-string () ; create string with fill pointer {{{
	(make-array '(0) :element-type 'base-char :fill-pointer 0 :adjustable t)
) ; }}}

(defun list-to-string (lst) ; {{{
	;; https://gist.github.com/tompurl/5174818
	(format nil "~{~A~}" lst)
) ; }}}

(defun bytearray-to-string (bytes) ; {{{
	(let ((str (make-string (length bytes))))
	(loop for byte across bytes  for i from 0  do
		(setf (aref str i) (code-char byte)))
	str
)) ; }}}

(defun color-text (color str) "return terminal escape code" ; {{{
	;; https://stackoverflow.com/questions/19208933/change-the-color-of-the-text-in-the-common-lisp-repl
	(let ((color (cond
		((string= color "red") "31")
		((string= color "green") "32")
		((string= color "yellow") "33")
		((string= color "white") "37")
		((string= color "bright blue") "94")
		((string= color "bright yellow") "93")
		((string= color "bright cyan") "96")
		((string= color "bright magneta") "95")
		(t "90")
	)))
	
	(format nil "~c[~am~a~c[0m" #\ESC color str #\ESC) ; \e[31m Red \e[0
)) ; }}}

;;; }}}

;;; join string - join-str-with, join-str, join-lines {{{

(defun join-str-with (lines sep) ; join two or more strings {{{
	(let ((result "") (firststr (car lines)) (strlist lines) strlist-length)
	;; if passed a list, use that as the args list; else do nothing
	(when (equal (type-of firststr) 'cons)
		(setq strlist firststr))
	(setq strlist-length (- (length strlist) 1))
	
	;; for each item in strlist, add it onto result
	(loop for i  from 0 to strlist-length  do
		(let ((str (nth i strlist)) temp-sep)
		;; add separator only past first item
		(setq temp-sep
			(if (equal i 0)  "" sep))
		(when (null str)
			(setq str ""))
		(setq result (format nil "~a~a~a" result temp-sep str))
	))
	
	;; when testing out concatenate vs format on many many strings,
	;; it turned out format was notably faster, so I switched it here too
	
	result
)) ; }}}

(defun join-str (&rest strings) "join a string list into a string"
	(join-str-with strings ""))

(defun join-lines (&rest strings) "join a list of lines into a string"
	(join-str-with strings (nn)))

;;; }}}


;;; list related - string-member-p {{{

(defun cons-to (list-value cell-value)
	(cons cell-value list-value)
)

(defun string-member-p (str str-list) ; tell if a string is in a list {{{
	(let (result)
	(loop for  element in str-list  when (null result)  do
		(when (equal str element) (setq result element))
	)
	result
)) ; }}}

;;; }}}


;;; unix timestamp - universal-to-unix-time , unix-to-universal-time , get-unix-time {{{

(defvar *unix-epoch-difference*
	(encode-universal-time 0 0 0 1 1 1970 0))
(defun universal-to-unix-time (universal-time)
	(- universal-time *unix-epoch-difference*))
(defun unix-to-universal-time (unix-time)
	(+ unix-time *unix-epoch-difference*)
)
(defun get-unix-time () "return unix timestamp"
	;; https://lisptips.com/post/11649360174/the-common-lisp-and-unix-epochs
	(universal-to-unix-time (get-universal-time))
)

;;; }}}


;;; hash tables - puthash , debug-hash {{{

(defun puthash (key value table) "set a hash element (mimic emacs API)" ; {{{
	(setf (gethash key table) value)
) ; }}}

(defun debug-hash (table) "print contents of a hash table" ; {{{
	;; https://www.tutorialspoint.com/lisp/lisp_hash_table.htm
	(maphash #'(lambda (k v) (format t "~a => ~a~%" k v)) table)
) ; }}}

;;; }}}

