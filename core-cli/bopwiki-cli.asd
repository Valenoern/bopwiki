(defsystem "bopwiki-cli"
	:description "bop terminal interface"
	:author "Valenoern"
	:licence "GPL"
	
	:depends-on (
		"bopwiki"
			;; "uiop", etc
		"bop-rules"
			;; you should basically never Not Have This,
			;; but technically it will one day be possible to swap it out
	)
	
	:weakly-depends-on (
		;; bopwiki-test / "bop test" enables automated tests on bop's 'stock' systems
		;;   however, you might not have its dependency fiveam. so, only try to load it if you do
		;; LATER: this is a deprecated feature, also you can use test-op with a main system?
		;;   but I will only be able to mess with that later
		"bopwiki-test"
	)
	
	:components (
		(:file "terminal")  ; :bopwiki-terminal-interface2
))
