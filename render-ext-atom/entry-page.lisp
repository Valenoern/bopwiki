(defpackage :bop-render-atom-page ; {{{
	(:use :common-lisp :bopwiki :bop-render-core
		:bop-render-atom-author
	)
	(:local-nicknames
		(:table :bopwiki) (:render :bop-render-core)
		(:html :bop-render-html-copy)
		;; uiop
	)
	(:export
		#:atom-entry-tag #:atom-entry-title #:atom-entry-path #:atom-entry-published #:atom-entry-updated #:atom-entry-summary
		
		#:atom-tag-uri
		#:atom-entry-template
		#:render-entry-atom
))
(in-package :bop-render-atom-page)

(defstruct atom-entry ; {{{
	tag title path published updated summary
) ; }}}

;; }}}


(defun atom-entry-template (entry) ; atom entry xml string {{{
"Renders an individual atom feed entry as a string based on supplied properties."
	(format nil
"

<entry>
	<id>~a</id>
	<title>~a</title>
	<link rel=\"alternate\" type=\"text/html\" href=\"~a\" />
	<published>~a</published>
	<updated>~a</updated>
	<summary type=\"xhtml\">~a</summary>
</entry>"
		(atom-entry-tag entry)
		(atom-entry-title entry)
		(atom-entry-path entry)
		(atom-entry-published entry)  (atom-entry-updated entry)
		(atom-entry-summary entry)
	)
	;; LATER: allow passing in multiple links for multiple formats
) ; }}}

(defun format-body-xhtml (full-body) ; {{{
	(let ((EXCERPTED t))
	
	;; get page excerpt - copied from bop-render AP
	;; LATER: figure out at what step this should really go
	(when (null (bop-page-excerpt full-body))
		(setf (bop-page-excerpt full-body) (excerpt-text full-body))
	)
	
	(format nil
		;; this is how atom specifies a summary is html
		"<div xmlns=\"http://www.w3.org/1999/xhtml\">~a</div>"
		(html:format-body-html full-body EXCERPTED  :newline "&#10;" :use-title t)
		;; LATER: is cut link needed in feedreaders?
		;;   check that, & either omit or add absolute urls
	)
)) ; }}}

(defun render-entry-atom (pagename  &key body feed-meta) ; {{{
	(let (row  idno title  page-date update-date  entry-path tag  summary)
	
	;; get full body in order to get excerpt
	(let (full-body)
		;; mostly for testing, an already-fetched :body full-body can be passed in
		(setq full-body  (if (null body)
			(page-body pagename)  body
		))
		
		;; parse out summary, & run together for better indentation
		;; LATER: allow including entire entry as "content" rather than "summary"
		(setq summary (format-body-xhtml full-body))
		;; the rest of the atom entry only needs the metadata.
		(setq row (bop-page-tablerow full-body))
	)
	
	;;; dates
	(setq idno (bop-tablerow-idno row))
	;; published: should be got from entry display date rather than id number
	;;  as bop pages will frequently be given years-ago id numbers for organisation
	(setq page-date (epoch-to-iso idno))
	;; updated: should be calculated based on latest revision
	;; LATER: look through revisions
	(setq update-date page-date)
	
	;; bop page title based on entry nickname
	;; BUG:
	;; - this is not quite correct, should use header if present
	;; - instead of 'entry 1.01' format title shows as NIL when entry has no nickname
	(setq title (generated-title (bop-tablerow-title row) idno))
	
	;;; URIs
	;; tag: should distinguish entries across feeds without changing - nickname can't be used here.
	;; id can technically change sometimes, but I don't really care because nothing else distinguishes pages as reliably
	(setq tag (format nil "~a~a" (feed-meta-namespace feed-meta) idno))
	;; link: there can be one for each mimetype page is published in
	;;   enclosure: can be used for page attachments
	;; LATER: allow creating atom feeds + 'alternate' links for other formats than html.
	(setq entry-path (format nil "~a~a"
		(feed-meta-pagesdir feed-meta) (exported-page-url pagename "htm"))
	)
	
	;; render entry to xml string
	(atom-entry-template  (make-atom-entry ; {{{
		:tag tag  :title title  :path entry-path
		:published page-date  :updated update-date
		:summary summary
	)) ; }}}
	
	;; LATER:
	;; - atom supports "in-reply-to"; copy from activitystreams ver
	;; - author avatar with "icon"/"logo"
)) ; }}}
