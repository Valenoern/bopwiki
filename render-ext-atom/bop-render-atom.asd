(defsystem "bop-render-atom"
	:author "Valenoern"
;	:version "0.0.1"
	:licence "GPL"
	:description "bopwiki atom feed format"
	
	:depends-on ("uiop" "bopwiki" "bop-render" "bop-render-html")
	:components (
		;; package names: bop-render-atom-...
		
		(:file "feed-author")
		(:file "entry-page")
		
		(:file "render-atom" :depends-on ("feed-author" "entry-page"))
))
