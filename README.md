bopwiki (/'bop') is meant to be a simple "microwiki" implementation ─ a wiki-like system designed for recording very small thoughts and contextual connections between things.

the idea is to make something useful for concise summaries of complex knowledge, very simple issue trackers, a way to organise your mastodon threads, a place to draft ideas for longer webpages without publicly microblogging, etc.  
(a teeny bit like [tiddlywiki](https://tiddlywiki.com/), but much more friendly to opening raw pages in a separate text editor and not necessarily needing both an entire backend engine + browser to view.)

it's fairly similar to a [Zettelkasten](https://en.wikipedia.org/wiki/Zettelkasten) system.

(for more on "microwikis", see the original mastodon thread:)  
[unrolled](http://distributary.network/burningtree/2020-08-25_microwiki.gmi) / [original](https://floss.social/@Valenoern/104748723877868120)

---

# "Documentation"

## code files

bop has just been ported to a proper asdf system structure. This new code structure will be internally documented some time soon as it goes through the process of being fully cleaned up.

## working features

(possibly incomplete list)

* microwiki pages - simple text files named as their created time with optional metadata lines at bottom
  * open a new page in external editor - `bop new`
  * manually nickname a page outside of bop, e.g. "1602312899_test.txt"
  * list all pages visible from current directory - `bop list`
* renaming pages
  * changing page nickname - `bop rename test newname` renames 1602312899_test.txt to 1602312899_newname.txt
  * changing page date - `bop rename test 1412952899` renames 1602312899_test.txt to 1412952899_test.txt
* edit pages by either their ID or nickname - `bop open test` opens 1602312899_test.txt
* features for journalling
  * mark a page as today, or open it - `bop today pagename`, `bop today`
* render page + replies to simple webpage - `bop publish`
  * publish specific defined groups of directories to a format, or to multiple formats in sequence (extension)
  * render pages to atom feed (experimental)

## in-progress / planned features

Currently in-progress tasks are in the `TODO` file, and longer-term tasks are in the "issue tracker" folder, `issues.bop`.  
You can use `bop publish` in that directory and open `file:///tmp/issues-bop/index.htm` to see an overview.

(An end-user documentation thread is also under construction inside issues.bop, though far from finished - see "`0947217626_docs.htm`" in the `issues-bop` folder.)


## branches & updates

bop technically has two branches - my private branch `unstable`, and the public branch `main`.  
This is mainly so I can endlessly amend commits on `unstable` until I lock them in, 'release' them to my public forge page, and start the next update.  
so, if the README you see here looks suspiciously outdated, it's somewhat likely I'm sitting on an unfinished update that will release later, and you only need to be patient. (or bug me with questions)

bop is now _almost_ able to install all its dependencies using the `installer` directory.  
This is not quite ready, but when it is, you will be able to install a "development" version of bop on either manjaro/arch or debian using `make && make install`.

PKGBUILDs for building bop-related arch/debian packages live at [codeberg.org/...bop-ytp-pkgbuild](https://codeberg.org/Valenoern/bop-ytp-pkgbuild).  
There may also eventually be a regularly-updated place to get binary "non-development" packages (like a [PPA](https://itsfoss.com/ppa-guide/)), but my system for putting one up on a git host is still too experimental right now.


## License

bop is released under the GPL3 or later.  
(This was originally chosen partly so I could archive "[spectrom](https://codeberg.org/Valenoern/spectrom-demo)" into the bop history. That is a nearly-irrelevant bit of historical trivia now, although I may still do it.)

<span><br></span>

(This first half of the readme is currently outdated. it will be rewritten later)

---


# Contributing

## Active feature branches

bop's "official feature branches" are a bit of a mess currently, and still need to be cleaned up — each one of these feature branches has an inconsistent codebase and is practically its own small fork.  
The following feature branches are the most maintained and most likely to merge any time soon:

* `unst-alias` - an overhaul of the core "bopwiki" system to better support things like arbitrary filenames; see [Valenoern/bopwiki-unstable](https://codeberg.org/Valenoern/bopwiki-unstable)
* `unst-tracks` - an experiment to read and display ID3 metadata information for the purpose of showing off music tracks in entry threads or playlists; see [Valenoern/bopwiki-unstable](https://codeberg.org/Valenoern/bopwiki-unstable)

The feature branch names have been added to this repository pointing to nothing in particular in order to mention them in issue tracker entries.


## Online issue tracker

In parallel to `issues.bop`, bop has an issue tracker on Codeberg (a [Forgejo](https://codeberg.org/forgejo/forgejo) installation). This is to make it a little easier for developers outside the [v-refracta team](https://codeberg.org/vxsh-suite) to contribute issues. (Admittedly, not maximally easy when there is no official email account or other such contact method one can use without registering for Codeberg; this may be addressed in the future.)

The online issue tracker has the following labels:

* `Kind/Bug` - a problem with running or using the program; bad design is considered a bug
* `Kind/Feature` - a functionality suggestion which is not a bug
* `Kind/Documentation` - for topics that really should be explained in `issues.bop` or within source code but are not; for any legitimate questions you have about how to understand the program
* `Kind/Testing` - an issue related to automated testing; may also be marked `Kind/Bug` if a test is broken
* `Reviewed/Confirmed` - an issue that is reproducible or has no problems
* `Reviewed/Duplicate` - an issue which repeats an existing issue
* `Reviewed/Invalid` - an issue that cannot be addressed in its current form for some reason
* `Priority/High` - for issues that are likely to be addressed soon
* `Priority/Low` - for issues that are only likely to be addressed in the future
* `Status/Abandoned` - there is no active work on the solution
* `Status/Blocked` - the solution is obstructed by something
* `Status/Need More Info` - an issue that is neither confirmed nor invalid, and needs more information to be addressed

The following default labels suggested by Forgejo have been omitted:

* `Kind/Breaking` - this suggests the issue tracker should be used as a long-term historical record of breaking changes, which I do not think is a realistic way to use decentralised community-run platforms; instead of applying this label, breaking changes should be added to the record of breaking changes in `issues.bop`
* `Kind/Enhancement` - use `Kind/Feature` or `Kind/Bug`
* `Kind/Security` - if there is any issue serious enough to be marked Security, it should be added to the summary of changes in `issues.bop` if/when fixed, and possibly directly sent to members of the [v-refracta team](https://codeberg.org/vxsh-suite); it is unlikely bop has any issues of this kind
* `Priority/Critical` - use `Priority/High`. bop as a project does not really have any issues that can be addressed with "Critical" speed
* `Priority/Medium` - use either `Priority/Low` or `Priority/High` to suggest whether the issue should actively be worked on
* `Reviewed/Won't Fix` - use `Reviewed/Invalid`. I (Valenoern) have always hated it when projects arbitrarily label something WONTFIX, so I think one should have to commit to either including that category of issue in the definition of Invalid, or giving conditions under which the project could devise a fix.

Regardless of any online issue trackers, note that `issues.bop` is supposed to be the more definitive record of current and completed tasks for offline reference. Whenever an issue is sufficiently significant, it will be copied or paraphrased to an `issues.bop` entry as a clean record of what the actual task is without any extraneous information one might find in a discussion thread.

