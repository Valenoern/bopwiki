#!/bin/bash
shared=/usr/share/bopwiki
branch=$1

if [[ -L "$shared" || ! -e "$shared" ]]; then
  if [[ -L "$shared" ]]; then
    echo "Removing $shared"
    sudo rm "$shared"
  fi
  
  echo "Creating link to $branch at $shared"
  sudo ln -s "$branch" "$shared"
  echo "Now using branch at $branch"
fi
