;;; bluebird.lisp - library to convert unix seconds to "afraid bluebird" {{{
(defpackage :bopwiki-bluebird
	(:use :common-lisp)
	(:local-nicknames
		;;(:pcre :cl-ppcre)
		(:eff :bopwiki-bluebird-list-eff)
	)
	(:export
		#:to-arbitrary-base  ; #::largest-place
		;; #::digit-to-word #::number-to-words
		#:epoch-to-bluebird
		
		#:bluebird-plainly #:bluebird-numeric
))
(in-package :bopwiki-bluebird)
;;; }}}


;;; base functions {{{

(defun largest-place (input radix) ; largest exponent in number {{{
"determine the largest \"place\" (power) that RADIX can be raised to while representing INPUT number"
	;; loop through possible places one by one
	(do ((place 0 (+ place 1)))
		;; stop if RADIX ^ PLACE is bigger than input 
		((>=  (mod input (expt radix place))  input)
		;; MOD returns a smaller number if successfully divided and the input number when unsuccessful, so stop when the input number is returned
			(- place 1))
)) ; }}}

(defun to-arbitrary-base (input radix) ; express big number as list {{{
;; sadly, native common lisp functions only go up to base 36, while I needed base 7776
	(let ((divided input) result
		(largest-power (largest-place input radix))
	)
	
	;; construct number in reverse order
	(loop for power  from largest-power downto 0  do
		(let ((place-maximum (expt radix power)) place)
		
		(setq place (truncate divided place-maximum))
		(setq divided (- divided (* place-maximum place)))
		
		(setq result
			(cons place result))
	))
	
	(reverse result)  ; return arbitrary-base number as a list
)) ; }}}

;;; }}}

(defun digit-to-word (decimal word-list) ; look up word on digit list {{{
	;; assumes the list will be a vector
	;; argument order modelled on 'nth and 'get-hash
	
	(aref word-list decimal)
) ; }}}

(defun number-to-words (number-list word-list) ; numeric list to word list {{{
;; assumes NUM is list
	(let (result)
	
	(dolist (digit number-list)
		(setq result
			(cons (digit-to-word digit word-list) result))
	)
	(reverse result)
)) ; }}}

(defun epoch-to-bluebird (epoch  &key word-list) ; {{{
"convert unix second integer EPOCH to its huge-base / \"afraid bluebird\" representation using WORD-LIST for digits"
	
	;; if not given another word list, use provided one
	(when (null word-list)
		(setq word-list (eff:eff-large-wordlist)))
	
	;; the main step is to convert the number to expanded notation on (LENGTH WORD-LIST)
	;; 1637999800 -> (27 * 7776^2) + (696 * 7776) + 952
	
	(number-to-words  ; look up each place value to get word
		;; convert EPOCH to expanded notation on really big base, like 7776
		(to-arbitrary-base epoch (length word-list))  ; ex.: (27 696 952)
		word-list)
) ; }}}


;; LATER: combine redundant parts of functions

(defun bluebird-plainly (epoch  &key words) ; {{{
	(let (
		(bluebird-words
			(if (null words) (epoch-to-bluebird epoch) words)
		)
		(result-string "")
	)
	
	(dolist (word bluebird-words)
		(let (
			(separator
				(if (equal result-string "")  ""  " "))
		)
		(setq result-string (format nil "~a~a~a" result-string separator word))
	))
	
	(values result-string bluebird-words)
)) ; }}}

(defun bluebird-numeric (epoch  &key words) ; {{{
	(let (
		(bluebird-words
			(if (null words) (epoch-to-bluebird epoch) words)
		)
		(result-string "") (id-string (format nil "~a" epoch)) numbers
	)
	
	(dolist (word bluebird-words)
		(let (
			(separator
				(if (equal result-string "")  ""  "-"))
		)
		(setq result-string (format nil "~a~a~a" result-string separator word))
	))
	
	(let ((len (length id-string)))
		(setq numbers
			(if (> len 3)
				(format nil " ~a,~a"
					(elt id-string (- len 3))
					(subseq id-string (- len 2))
				)
				""))
	)
	
	(values
		(format nil "~a~a" result-string numbers)  bluebird-words)
)) ; }}}
