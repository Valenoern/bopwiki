(defsystem "bop-render-publishgroup"
	:author "Valenoern"
;	:version "0.0.1"
	:licence "GPL"
	:description "publish groups"
	
	:depends-on ("uiop" "bopwiki" "bop-render")
	:components (
		(:file "publishgroup")
))
